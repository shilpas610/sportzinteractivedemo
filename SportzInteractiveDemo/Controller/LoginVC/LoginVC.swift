//
//

import UIKit

class LoginVC: UIViewController {
   
    // MARK: - IBOutlet's
        

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var backView: UIView!

        
    // MARK: - Properties
   
    lazy var loginViewModel: LoginViewModel = {
            let loginViewModel = LoginViewModel()
            return loginViewModel
    }()
    
    
    // MARK: - View Controllers Life Cycle
   
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.bindViewModel()

    }
    

}
// MARK: - IBAction's

extension LoginVC
{
    // MARK: - Login button tapped
    @IBAction func didTapOnLoginButton(_ sender: UIButton) {
        self.loginViewModel.requestLogin(emailTextField.text, passwordTextField.text)
        
    }

    
    @IBAction func didTapOnSkipButton(_ sender: UIButton) {
        
        self.gotoHomeScreen()
        
    }
}


// MARK:Helpers

extension LoginVC{

    
    private func bindViewModel(){
        
        self.loginViewModel.errorMessage.addObserver(fireNow: false) { [weak self] (errorMessage) in
            
            self?.showErrorMessage(errorMessage!)
        }
        
        self.loginViewModel.isLoginSuceess.addObserver(fireNow: false) { (isSuccess) in
            guard let isSuccess = isSuccess else {return}
            if isSuccess{
                self.showProgress()
                DispatchQueue.main.asyncAfter(deadline: .now()+1){
                    self.hideProgress()

                    self.gotoHomeScreen()
                }
               
            }
            
        }
    }
 
    private func showErrorMessage(_ message: String) {
        self.showAlert(withTitle: Constants.alertErrorTitle, withMessage: message)
        
    }
    
    
    private func initializeFromStoryboard() -> LoginVC {
        let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: LoginVC.storyboardID) as! LoginVC
        return controller
    }
    
    private func gotoHomeScreen(){
        DispatchQueue.main.async
            {
                
                
                let controller = HomeVC().initializeFromStoryboard()
                
                controller.providesPresentationContextTransitionStyle = true
                controller.definesPresentationContext = true
                
            controller.modalPresentationStyle = UIModalPresentationStyle.fullScreen;
                if #available(iOS 13.0, *) {
                    controller.isModalInPresentation = true
                } else {
                    // Fallback on earlier versions
                }
                self.present(controller, animated: true, completion: nil)
                
        }
        
    }

    
}
