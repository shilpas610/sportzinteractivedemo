//
//  PlayerDetailsTableCell.swift
//  SportzInteractiveDemo
//
//  Created by UpperthrustiOS on 08/01/21.
//

import UIKit

class PlayerDetailsTableCell: UITableViewCell {
    
    // MARK: - IBOutlet's
        

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var isCaptainLabel: UILabel!

    @IBOutlet weak var battingStyleLable: UILabel!
    @IBOutlet weak var battingAvgLable: UILabel!
    @IBOutlet weak var battingStrikeRateLable: UILabel!
    @IBOutlet weak var battingRunsLable: UILabel!
   
    
    @IBOutlet weak var bowlingStyleLable: UILabel!
    @IBOutlet weak var bowlingAvgLable: UILabel!
    @IBOutlet weak var bowlingEconomyLable: UILabel!
    @IBOutlet weak var bowlingWickets: UILabel!
    
    
    
    // MARK: - Properties

    
    lazy var playerModel:Player? = nil{
        didSet{
            guard let player = playerModel else {return}
            
            DispatchQueue.main.async {
                [weak self] in
                
                self?.nameLabel.text = player.nameFull
 
                if let _ = player.iscaptain{
                    self?.isCaptainLabel.text = "(Captain)"
                }
                else if let _ = player.iskeeper{
                    self?.isCaptainLabel.text = "(Wicket Keeper)"
                }
                else{
                   
                    self?.isCaptainLabel.text = nil
                }
                

                
                self?.battingStyleLable.text = (player.batting?.style).map { $0.rawValue }
              
                self?.battingAvgLable.text = player.batting?.average
               
                self?.battingStrikeRateLable.text = player.batting?.strikerate
                self?.battingRunsLable.text = player.batting?.runs
 
                
                self?.bowlingStyleLable.text = player.bowling?.style
                
                self?.bowlingAvgLable.text = player.bowling?.average
               
                self?.bowlingEconomyLable.text = player.bowling?.economyrate
                self?.bowlingWickets.text = player.bowling?.wickets
                
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
