

import UIKit

class DashboardVC: UIViewController {
   
    
    // MARK: - IBOutlet's
        


    @IBOutlet weak var btnScorchers: UIButton!
    @IBOutlet weak var lblGrayScor: UILabel!
    @IBOutlet weak var lblGrayHeat: UILabel!
    @IBOutlet weak var lblScorchers: UILabel!
    @IBOutlet weak var btnHeat: UIButton!
    @IBOutlet weak var lblHeat: UILabel!
    @IBOutlet weak var tableViewHeat: UITableView!
    
    
    
    // MARK: - Properties
   
    lazy var dashboardViewModel: DashboardViewModel = {
            let dashboardViewModel = DashboardViewModel()
            return dashboardViewModel
    }()
    
    lazy var playerModel:[Player]? = nil{
        didSet{
            DispatchQueue.main.async {
                [weak self] in
                self?.tableViewHeat.reloadData()
            }
        }
    }
    
    
    // MARK: - View Controllers Life Cycle
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        self.bindViewModel()

        lblScorchers.backgroundColor = #colorLiteral(red: 0.6117647059, green: 0.7529411765, blue: 0.3411764706, alpha: 1)
        lblScorchers.isHidden = false
        
        self.btnHeat.setTitleColor(UIColor.lightGray, for: .normal)
        lblHeat.isHidden = true
        lblGrayHeat.isHidden = false
    

    }
  

}
// MARK: - IBAction's

extension DashboardVC
{
    
    // MARK: - Scorchers button tapped
    
    @IBAction func actionOnBtnScorchers(_ sender: UIButton) {
        
        
        
        self.btnScorchers.setTitleColor(UIColor.systemBlue, for: .normal)
        lblScorchers.backgroundColor = #colorLiteral(red: 0.6117647059, green: 0.7529411765, blue: 0.3411764706, alpha: 1)
        lblScorchers.isHidden = false
        
        self.btnHeat.setTitleColor(UIColor.lightGray, for: .normal)
        lblHeat.isHidden = true
        lblGrayHeat.isHidden = false
        
        
        guard let player = self.dashboardViewModel.playersSortedModel.value?[1] else{return}
        
        self.playerModel = player
    }
    
    // MARK: - Heat button tapped
    @IBAction func actionOnBtnHeat(_ sender: UIButton) {
        
        self.btnHeat.setTitleColor(UIColor.systemBlue, for: .normal)
        lblHeat.backgroundColor = #colorLiteral(red: 0.6117647059, green: 0.7529411765, blue: 0.3411764706, alpha: 1)
        lblHeat.isHidden = false
        self.btnScorchers.setTitleColor(UIColor.lightGray, for: .normal)
        lblScorchers.isHidden = true
        lblGrayScor.isHidden = false
        
        guard let player = self.dashboardViewModel.playersSortedModel.value?[0] else{return}
        
        self.playerModel = player


       
    }
    
    @IBAction func actionOnBtnBackButton(_ sender: UIButton) {
        DispatchQueue.main.async
            {
                
                
                let controller = HomeVC().initializeFromStoryboard()
                
                controller.providesPresentationContextTransitionStyle = true
                controller.definesPresentationContext = true
                
            controller.modalPresentationStyle = UIModalPresentationStyle.fullScreen;
                if #available(iOS 13.0, *) {
                    controller.isModalInPresentation = true
                } else {
                    // Fallback on earlier versions
                }
                self.present(controller, animated: true, completion: nil)
                
        }
        
    }
    
}

// MARK:Helpers

extension DashboardVC{

    
    private func setupTableView() {
 
        self.tableViewHeat.registerCell(type: PlayerDetailsTableCell.self)
        
        self.tableViewHeat.dataSource = self
        self.tableViewHeat.delegate = self
        self.tableViewHeat.tableFooterView = UIView()
        
        self.btnScorchers.setTitleColor(UIColor.lightGray, for: .normal)
        self.btnHeat.setTitleColor(UIColor.lightGray, for: .normal)
        


  
    }
    
    private func bindViewModel(){
        
        self.dashboardViewModel.requestDashboard()
        
        self.dashboardViewModel.cricketResponseModel.addObserver(fireNow: false) { (response) in
            guard let response = response else {return}
            

            self.dashboardViewModel.getPlayersDetail(resp: response)
        
        }
        self.dashboardViewModel.playersSortedModel.addObserver(fireNow: false) { (playersArray) in
            guard let sortedPlayers = playersArray else {return}
            
        
            
            if sortedPlayers.count > 0{
                self.playerModel = sortedPlayers[1]
            }
            
            
           
        }
    }
 
    private func showErrorMessage(_ message: String) {
        self.showAlert(withTitle: Constants.alertErrorTitle, withMessage: message)
        
    }
    
    
    func initializeFromStoryboard() -> DashboardVC {
        let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: DashboardVC.storyboardID) as! DashboardVC
        return controller
    }
    
    
}

