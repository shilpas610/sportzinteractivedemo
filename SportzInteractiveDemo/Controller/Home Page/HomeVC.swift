//
//  HomeVC.swift
//  SportzInteractiveDemo
//
//

import UIKit

class HomeVC: UIViewController {
    
    // MARK: - IBOutlet's
        
    @IBOutlet weak var tourNameLabel: UILabel!
    @IBOutlet weak var venueName: UILabel!
    @IBOutlet weak var teamOneNameLabel: UILabel!
    @IBOutlet weak var teamTwoNameLabel: UILabel!
    @IBOutlet weak var homeTitleLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var imageView: UIView!
    @IBOutlet weak var statusLabel: UILabel!
    
    
    
    // MARK: - Properties
   
    lazy var dashboardViewModel: DashboardViewModel = {
            let dashboardViewModel = DashboardViewModel()
            return dashboardViewModel
    }()

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageView.isHidden = true
        self.backView.isHidden = true
        self.bindViewModel()
    }
    

}
// MARK: - IBAction's

extension HomeVC
{
    
        @IBAction func didTappedOnDetailButton(_ sender: UIButton) {
            self.gotoDashboard()
        }
      
}


// MARK:Helpers

extension HomeVC{

    private func bindViewModel(){
        
        self.dashboardViewModel.requestDashboard()
        
        self.dashboardViewModel.cricketResponseModel.addObserver(fireNow: false) { (response) in
            guard let response = response else {return}
    
            
            self.setUpUI(response: response)

        
        }
        self.dashboardViewModel.isLoading.addObserver(fireNow: false) { (isLoader) in isLoader ? (self.showProgress()) : (self.hideProgress())
        }
    }
    
    private func setUpUI(response:CricketResponseModel){
        guard let teamOneId = response.matchdetail?.teamHome, let teamTwoId = response.matchdetail?.teamAway else {return}
        
        if let tourName = response.matchdetail?.series?.tourName{
            self.tourNameLabel.text = tourName
        }
        if let venueName = response.matchdetail?.venue?.name{
            self.venueName.text = venueName
        }
        if let teamOne = response.teams?.first?.value.nameFull{
            self.teamOneNameLabel.text = teamOne
        }
        if let teamOne = response.teams?[teamOneId]?.nameFull,let teamTwo = response.teams?[teamTwoId]?.nameFull{
            self.teamOneNameLabel.text = teamOne
            self.teamTwoNameLabel.text = teamTwo
        }
        if let hometitle = response.matchdetail?.match?.league{
            self.homeTitleLabel.text = hometitle.uppercased()
        }
        
        if let result = response.matchdetail?.result{
            self.statusLabel.text = result
        }
        DispatchQueue.main.asyncAfter(deadline: .now()+1){
            self.imageView.isHidden = false
            self.backView.isHidden = false
            self.hideProgress()
            
        }
    }
 
    private func showErrorMessage(_ message: String) {
        self.showAlert(withTitle: Constants.alertErrorTitle, withMessage: message)
        
    }
    
    private func gotoDashboard(){
        DispatchQueue.main.async
            {
                
                
                let controller = DashboardVC().initializeFromStoryboard()
                
                controller.providesPresentationContextTransitionStyle = true
                controller.definesPresentationContext = true
                
            controller.modalPresentationStyle = UIModalPresentationStyle.fullScreen;
                if #available(iOS 13.0, *) {
                    controller.isModalInPresentation = true
                } else {
                    // Fallback on earlier versions
                }
                self.present(controller, animated: true, completion: nil)
                
        }
        
    }
    
    
    func initializeFromStoryboard() -> HomeVC {
        let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: HomeVC.storyboardID) as! HomeVC
        return controller
    }
    
    
}

