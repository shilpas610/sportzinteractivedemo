//
//  LoginViewModel.swift
//  SportzInteractiveDemo
//



import UIKit
import Alamofire


protocol LoginViewModelProtocol:AnyObject {
    
    
    var isLoginSuceess:Observable<Bool?> {get set}
 
    var isLoading:Observable<Bool>{get}
    var errorMessage:Observable<String?> {get set}
    var error: Observable<Bool> { get set }

    func requestLogin(_ email:String?,_ password:String?)

    func setError(_ message:String)
    
}


public class LoginViewModel:LoginViewModelProtocol{
    var isLoginSuceess: Observable<Bool?> = Observable(value: false)
    
    var isLoading: Observable<Bool> = Observable(value: false)
    
    var errorMessage: Observable<String?> = Observable(value: nil)
    
    var error: Observable<Bool> = Observable(value: false)
    
    func requestLogin(_ email: String?, _ password: String?) {
        
        if let email = email{
           if email.isEmpty{
                self.setError(Constants.emptyEmail)
                return
            }
        }
        if let password = password{
           if password.isEmpty{
                self.setError(Constants.emptyPassword)
                return
            }
        }
        
        if email == "charmi@sportzinteractive.net" && password == "pass123"{
            self.isLoginSuceess.value = true
        }
        else{
            self.setError(Constants.validEmailPassword)
        }
    }
    
    func setError(_ message: String) {
        
        self.errorMessage.value = message
        self.error.value = true

    }
    
    
}

