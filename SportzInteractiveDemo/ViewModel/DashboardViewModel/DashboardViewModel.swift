//
//  DashboardViewModel.swift
//  SportzInteractiveDemo


import UIKit
import Alamofire



protocol DashboardViewModelProtocol:class {
    
    
    var cricketResponseModel:Observable<CricketResponseModel?> {get set}
    var playersSortedModel:Observable<[[Player]]?> {get set}
 
    var isLoading:Observable<Bool>{get}
    var errorMessage:Observable<String?> {get set}
    var error: Observable<Bool> { get set }

    func requestDashboard()

    func setError(_ message:String)
    
}


public class DashboardViewModel:DashboardViewModelProtocol{
    var playersSortedModel: Observable<[[Player]]?> = Observable(value: nil)
    
    var cricketResponseModel: Observable<CricketResponseModel?> = Observable(value: nil)
    
    var isLoading: Observable<Bool> = Observable(value: false)
    
    var errorMessage: Observable<String?> = Observable(value: nil)
    
    var error: Observable<Bool> = Observable(value: false)
    
    var repository:DashboardRepository?
    
    init(repository:DashboardRepository = APIDashboardRepository()) {
        
        self.repository = repository
 
    }
    
    
    
    func requestDashboard() {
        
        self.isLoading.value = true
        
        
        self.repository?.getDashboardBottomResponse(completion: { (response:DataResponse<CricketResponseModel?, AFError>) in
            
            self.isLoading.value = false
            
            switch response.result{
                case .success(let model):
                    guard let model = model else {
                        self.setError(BaseNetworkManager().getErrorMessage(response: response))
                        return
                    }
                    self.cricketResponseModel.value = model
            case .failure:
                self.setError(BaseNetworkManager().getErrorMessage(response: response))
            }
        })


    }
    
    func setError(_ message: String) {
        
        self.errorMessage.value = message
        self.error.value = true

    }
    
    ///get teams from response
    
    func getPlayersDetail(resp:CricketResponseModel?){
        
        guard let response = resp else{return}
        
        if let teams = response.teams{
            
            self.playersSortedModel.value = teams.compactMap{$0.value.players?.compactMap{$0.value}}
            
            
        }
        else{
            self.playersSortedModel.value = nil
        }
    }
}

