//
//  BaseNetworkManager.swift
//  SportzInteractiveDemo
//

//

import Foundation
import Alamofire

public class BaseNetworkManager
{
    
    public func getErrorMessage<T>(response: DataResponse<T, AFError>)->String where T:Codable
    {
        var message = NetworkingConstants.networkErrorMessage
        
        
        
        if let data = response.data
        {
            if let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
            {
                if let error = json["errors"] as? NSDictionary
                {
                    message = error["message"] as! String
                }
                else if let error = json["message"] as? NSDictionary
                {
                    if let message1 = error["message"] as? String
                    {
                       message = message1
                    }
                    
                }
            }
        }
        else{
            if let underlyingError = response.error?.underlyingError{
                if let urlError = underlyingError as? URLError {
                    switch urlError.code {
                    case .timedOut:
                        message = "Timed out error"
                    case .notConnectedToInternet:
                        message = NetworkingConstants.networkErrorMessage
                    case .networkConnectionLost:
                        message = "Network Connection Lost"
                    default:
                        print("Unmanaged error")
                    }
                }
            }
            
        }

        return message
    }
    
}




struct NetworkingConstants {
    
  
   static let baseUrl = "https://demo.sportz.io/"//"https://cricket.yahoo.net/sifeeds/cricket/live/json/"
 
    static let networkErrorMessage = "Please check your internet connection and try again."
}

struct ApiEndpoints
{
    static let cricketLive = "sapk01222019186652.json"//"nzin01312019187360.json"
}



protocol APIConfiguration:URLRequestConvertible {
    var method : HTTPMethod {get}
    var path : String {get}
    var headers : HTTPHeaders? {get}
    var parameters : Parameters? {get}
}

enum NewtwokError : LocalizedError{
    
    case responseStatusError(message:String)
    
}


extension NewtwokError{
    var errorDescription: String?{
        switch self {
        case let .responseStatusError(message):
            return "\(message)"
        }
    }
    
}


extension JSONDecoder {
    func decodeAndCheck<T>(_ type: T.Type, from data: Data) throws -> T where T : Decodable  {
       let result = try self.decode(type, from: data)

        if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? [String: Any] {
            let mirror = Mirror(reflecting: result)
            let jsonKeys = json.map { return $0.0 }
            let objectKeys = mirror.children.enumerated().map { $0.element.label }

            jsonKeys.forEach { (jsonKey) in
                if !objectKeys.contains(jsonKey) {
                    print("\(jsonKey) is not used yet")
                }
            }
        }
        return result

    }
}
