//
//  APIRouter.swift
//  SportzInteractiveDemo
//


import Foundation
import Alamofire


enum APIRouter : APIConfiguration{
    
    
    case cricketLiveRequest
   
    internal var method: HTTPMethod{
        switch self {
            case .cricketLiveRequest: return .get
        }
    }
    
    internal var path: String{
        
        switch self {
        
        case .cricketLiveRequest: return NetworkingConstants.baseUrl + ApiEndpoints.cricketLive
            
 
        }
    }
    
    var parameters: Parameters?{
        switch self {
            case .cricketLiveRequest:
                return nil
        
        }
    }

    
    internal var body: [String : Any]{
        switch self
        {
        default: return [:]
        }
    }


   internal var headers: HTTPHeaders?{
       switch self {
       case .cricketLiveRequest:
        return  ["Content-Type": "application/json", "Accept": "application/json"]
        
       }
      
   }
   
    
   func asURLRequest() throws -> URLRequest {
    var urlComponents = URLComponents(string:path)!
    var queryItems: [URLQueryItem] = []
    
        if let parameters = parameters{
            for item in parameters
            {
                queryItems.append(URLQueryItem(name: item.key, value: "\(item.value)"))
            }

        }
    
        if !(queryItems.isEmpty){
            urlComponents.queryItems = queryItems
            
        }
    
        let url = urlComponents.url!
        var urlRequest = URLRequest(url: url)
       
        urlRequest.httpMethod = method.rawValue
       
        if let headers = headers{
        urlRequest.allHTTPHeaderFields = headers.dictionary

        }
  
        if !(body.isEmpty) {
            urlRequest = try URLEncoding().encode(urlRequest, with: body)
            let jsonData1 = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
            urlRequest.httpBody = jsonData1
        }
        return urlRequest

    
    }
    
    
}
