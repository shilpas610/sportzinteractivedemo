//
// 

import Foundation
import Alamofire

protocol DashboardRepository {

    func getDashboardBottomResponse(completion: @escaping (DataResponse<CricketResponseModel?, AFError>) -> Void)

}

public class APIDashboardRepository:DashboardRepository{

    

    

    private let session: Session
    
    init(_ session:Session = Session.default) {
        self.session = session
    }
    
    
    func getDashboardBottomResponse(completion: @escaping (DataResponse<CricketResponseModel?, AFError>) -> Void) {
        session.request(APIRouter.cricketLiveRequest).responseDecodable { (response) in
            completion(response)
        }
    }
    

}
