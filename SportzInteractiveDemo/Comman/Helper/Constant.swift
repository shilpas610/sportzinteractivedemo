//
//  Constant.swift
//  SportzInteractiveDemo
//
//

import Foundation



struct Constants {
    //Alert Constant
    
    static let alertErrorTitle = "Error"
    static let alertDefaultTitle = "Alert"
    
    //Login Screen Constant
    static let emptyEmail = "Please Enter Email Id."
    static let emptyPassword = "Please Enter Password Id."
    static let validEmailPassword = "Please Enter valid EmailID/Password."

}
